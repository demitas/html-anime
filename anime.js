// get random number to force reload
function get_dev_random_number() {
  let _script = document.getElementById("_anime");
  if (_script) {
    console.log(_script.src);
    let re = /.*[?&]+dev=([^&]*)/;
    let match = re.exec(_script.src);
    if (match) {
      return match[1];
    }
  }
  return "0";
}

function anime() {
  let _dev = get_dev_random_number();
  console.log(_dev);

  let _ticker = 0;
  let animation_list = [];
  let canvas = document.getElementById("canvas");

  let scale = 2;
  let spriteWidth = 32;
  let spriteHeight = 32;
  let spriteSheet = document.getElementById("SpriteSheet");
  let sheetWidth = spriteSheet.naturalWidth;
  let sheetHeight = spriteSheet.naturalHeight;

  register_anime(
    "sprite_idle_down",
    [
      [ 0,  0],
      [ 0,  1],
    ],
    4);

  register_anime(
    "sprite_run_down",
    [
      [ 0, 1],
      [ 0, 2],
      [ 0, 1],
      [ 0, 3],
    ],
    1);

  register_anime(
    "sprite_idle_left",
    [
      [ 1,  0],
      [ 1,  1],
    ],
    4);

  register_anime(
    "sprite_run_left",
    [
      [ 1, 1],
      [ 1, 2],
      [ 1, 1],
      [ 1, 3],
    ],
    1);

  let timer_id = setInterval(render_frame, 100);
  //clearInterval(timer_id);  // to stop animation timer

  function sprite_anime(element_id, ofs, tick_ratio) {
    let url_png = spriteSheet.src;

    let spr = document.getElementById(element_id);
    let tick1 = Math.floor(_ticker / tick_ratio) % ofs.length;
    let ofs_x = ofs[tick1][0] * spriteWidth * scale;
    let ofs_y = ofs[tick1][1] * spriteHeight * scale;
    spr.style.background = `url(${url_png}?dev=${_dev}) ${- ofs_x}px ${- ofs_y}px`;
    spr.style.backgroundSize = `${sheetWidth * scale}px ${sheetHeight * scale}px`;
  }

  function register_anime(element_id, ofs, tick_ratio) {
    let e = document.createElement("div");
    e.className = "sprite";
    e.id = element_id;
    e.style.width = `${spriteWidth * scale}px`;
    e.style.height = `${spriteHeight * scale}px`;
    canvas.append(e);
    animation_list.push(
      () => {
        sprite_anime(element_id, ofs, tick_ratio);
      }
    );
  }

  function render_frame() {
    _ticker += 1;
    _ticker %= 4294967296;  // prevent overflow
    for (animation of animation_list) {
      animation();
    }
  }
}
